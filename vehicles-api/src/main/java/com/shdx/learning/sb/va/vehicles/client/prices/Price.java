package com.shdx.learning.sb.va.vehicles.client.prices;

import java.math.BigDecimal;

/**
 * Represents the prices of a given vehicle, including currency.
 */
public class Price {

    private String currency;
    private BigDecimal price;
    private Long vehicleid;

    public Price() {
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getVehicleId() {
        return vehicleid;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleid = vehicleId;
    }
}
