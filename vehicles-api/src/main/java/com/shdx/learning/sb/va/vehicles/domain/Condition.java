package com.shdx.learning.sb.va.vehicles.domain;

/**
 * Available values for condition of a given car.
 */
public enum Condition {
    USED,
    NEW;
}
