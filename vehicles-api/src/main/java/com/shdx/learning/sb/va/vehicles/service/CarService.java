package com.shdx.learning.sb.va.vehicles.service;

import com.shdx.learning.sb.va.vehicles.client.maps.MapsClient;
import com.shdx.learning.sb.va.vehicles.client.prices.PricesClient;
import com.shdx.learning.sb.va.vehicles.domain.car.Car;
import com.shdx.learning.sb.va.vehicles.domain.car.CarRepository;
import java.util.List;

import org.springframework.stereotype.Service;

/**
 * Implements the car service create, read, update or delete
 * information about vehicles, as well as gather related
 * location and prices data when desired.
 */
@Service
public class CarService {

    private final CarRepository repository;
    private final MapsClient mapsClient;
    private final PricesClient pricesClient;

    public CarService(CarRepository repository, MapsClient mapsClient, PricesClient pricesClient) {
        this.repository = repository;
        this.mapsClient = mapsClient;
        this.pricesClient = pricesClient;
    }

    /**
     * Gathers a list of all vehicles
     * @return a list of all vehicles in the CarRepository
     */
    public List<Car> list() {
        return repository.findAll();
    }

    /**
     * Gets car information by ID (or throws exception if non-existent)
     * @param id the ID number of the car to gather information on
     * @return the requested car's information, including location and prices
     */
    public Car findById(Long id) {
        Car car = repository.findById(id).orElse(null);

        if (car == null) {
            throw new CarNotFoundException("Car not found");
        }

        car.setLocation(mapsClient.getAddress(car.getLocation()));
        car.setPrice(pricesClient.getPrice(id));

        return car;
    }

    /**
     * Either creates or updates a vehicle, based on prior existence of car
     * @param car A car object, which can be either new or existing
     * @return the new/updated car stored in the repository
     */
    public Car save(Car car) {
        if (car.getId() != null) {
            return repository.findById(car.getId()).map(carToBeUpdated -> {
                carToBeUpdated.setDetails(car.getDetails());
                carToBeUpdated.setLocation(car.getLocation());
                carToBeUpdated.setCondition(car.getCondition());
                return repository.save(carToBeUpdated);
            }).orElseThrow(CarNotFoundException::new);
        }

        return repository.save(car);
    }

    /**
     * Deletes a given car by ID
     * @param id the ID number of the car to delete
     */
    public void delete(Long id) {
        if (repository.existsById(id)) {
            repository.deleteById(id);
        } else {
            throw new CarNotFoundException();
        }
    }
}
