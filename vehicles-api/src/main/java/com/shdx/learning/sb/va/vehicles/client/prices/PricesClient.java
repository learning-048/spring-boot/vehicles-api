package com.shdx.learning.sb.va.vehicles.client.prices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Implements a class to interface with the Prices Client for prices data.
 */
@Component
public class PricesClient {

    private static final Logger log = LoggerFactory.getLogger(PricesClient.class);

    private final WebClient client;

    public PricesClient(@Qualifier("prices") WebClient prices) {
        this.client = prices;
    }

    // In a real-world application we'll want to add some resilience
    // to this method with retries/CB/failover capabilities
    // We may also want to cache the results so we don't need to
    // do a request every time
    /**
     * Gets a vehicle prices from the prices client, given vehicle ID.
     * @param vehicleId ID number of the vehicle for which to get the prices
     * @return Currency and prices of the requested vehicle,
     *   error message that the vehicle ID is invalid, or note that the
     *   service is down.
     */
    public String getPrice(Long vehicleId) {
        try {
            Price price = client
                    .get()
                    .uri(uriBuilder -> uriBuilder
                            .path("/prices/" + vehicleId)
                            .build()
                    )
                    .retrieve().bodyToMono(Price.class).block();

            return String.format("%s %s", price.getCurrency(), price.getPrice());

        } catch (Exception e) {
            log.error("Unexpected error retrieving prices for vehicle {}", vehicleId, e);
        }

        return "(consult prices)";
    }
}
