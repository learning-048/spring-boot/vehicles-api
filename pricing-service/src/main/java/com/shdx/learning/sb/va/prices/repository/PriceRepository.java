package com.shdx.learning.sb.va.prices.repository;

import com.shdx.learning.sb.va.prices.entity.Price;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PriceRepository extends CrudRepository<Price, Long> {

}
